import React from "react";
import { TableHeader, THead, THeadCell, THeadButton } from "@@components";

export const MainTableHead = () => {

    const colums = [
        {
            className: 'id', columName: 'ID'
        }, 
        {
            className: 'name', columName: 'ФИО'
        },
        {
            className: 'post', columName: 'Департамент и должность'
        },
        {
            className: 'role', columName: 'Роль'
        },
        {
            className: 'rules', columName: 'Глобальные права'
        }
    ];

    return (
        <TableHeader>
            <THead>
                {
                    colums.map((item: any) => (
                        <THeadCell 
                            key={item.className}
                            type={item.className}>
                            
                            <THeadButton>
                                {item.columName}
                            </THeadButton>
                        </THeadCell>
                    ))
                }
            </THead>
        </TableHeader>
    )
}