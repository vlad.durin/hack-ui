import React, { Fragment } from 'react';

import { MainTableHead } from './components';
import { PageWrapper } from '@@components';

export const Main = () => {
    return (
        <Fragment>
            <PageWrapper>
                <MainTableHead />
            </PageWrapper>
        </Fragment>
    )
}