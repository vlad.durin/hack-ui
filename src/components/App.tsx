import * as React from 'react';
import { Switch, Route, HashRouter } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';
import { LoginForm } from '@@components';
import { Main } from '../../src/pages/Main';

const App = () => (
    <HashRouter>
        <Switch>
            <Route exact path="/" component={LoginForm} />
            <Route path="/main" component={Main} />
        </Switch>
    </HashRouter>
);

export default hot(App);