import styled from 'astroturf';

const grey170 = '#D1D4DA';
const width = 1216;
const widthId = (136 / width) * 100;
const widthName = (438 / width) * 100;
const widthPost = (245 / width) * 100;
const widthRole = (218 / width) * 100;
const widthRules = (195 / width) * 100;

export const THeadCell: any = styled.li`
    width: 100px;
    height: 100%;
    position: relative;
    cursor: pointer;

    &:first-child {
        padding-left: 20px;
    }

    &:last-child {
        padding-right: 20px;
    }

    &.type-id {
        width: ${widthId}%;
    }

    &.type-name {
        width: ${widthName}%;
    }

    &.type-post {
        width: ${widthPost}%;
    }

    &.type-role {
        width: ${widthRole}%;
    }
    
    &.type-rules {
        width: ${widthRules}%;
    }

    &:hover {
        background-color: ${grey170};
        & > button {
            background-color: ${grey170};
        }
    }

    &.active-true {
        background-color: ${grey170};
        & > button {
            background-color: ${grey170};
        }
    }
`;