import styled from 'astroturf';

const grey120 = '#DDE0E6';
const grey170 = '#D1D4DA';

export const EmptyText: any = styled.p`
    height: 20px;
    border-radius: 2px;
    background-color: ${grey170};

    &.width-s {
        width: 52px;
    }

    &.width-m {
        width: 112px;
    }

    &.width-x {
        width: 158px;
    }

    &.width-xl {
        width: 211px;
    }

    &.type-subText {
        background-color: ${grey120};
    }
`;