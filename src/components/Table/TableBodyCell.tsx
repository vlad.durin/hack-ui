import styled from 'astroturf';

const width = 1216;
const widthId = (136 / width) * 100;
const widthName = (438 / width) * 100;
const widthPost = (245 / width) * 100;
const widthRole = (218 / width) * 100;
const widthRules = (195 / width) * 100;

export const TableBodyCell: any = styled.li`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    height: 100%;
    font-family: Roboto, Arial, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    box-sizing: border-box;
    padding-left: 8px;

    &.type-id {
        width: ${widthId}%;
    }

    &.type-name {
        width: ${widthName}%;

        :first-child {
            margin-right: 8px;
        }
    }

    &.type-post {
        width: ${widthPost}%;
        padding-left: 6px;
    }

    &.type-role {
        width: ${widthRole}%;
    }

    &.type-rules {
        width: ${widthRules}%;
    }

    &.column-true {
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;

        :first-child {
            margin-bottom: 4px;
        }
    }
`;