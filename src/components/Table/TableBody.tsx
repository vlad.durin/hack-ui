import styled from 'astroturf';

export const TableBody = styled.ul`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    list-style: none;
    min-height: 545px;
    position: relative;
    z-index: -1;
`;