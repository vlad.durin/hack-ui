import styled from 'astroturf';

export const TableButtonContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
`;