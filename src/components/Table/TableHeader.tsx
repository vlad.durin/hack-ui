import styled from 'astroturf';

const grey120 = '#DDE0E6';

export const TableHeader = styled.div`
    width: 100%;
    height: 48px;
    margin: 0 auto;
    background-color: ${grey120};
`;