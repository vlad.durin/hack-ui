import styled from 'astroturf';

const grey420 = '#8F939B';

export const Avatar = styled.figure`
    width: 32px;
    height: 32px;
    border-radius: 50%;
    background-color: ${grey420};
    box-shadow: 0px 0px 0px 2px rgba(0, 0, 0, 0.3);
    overflow: hidden;
`;
