import styled from 'astroturf';

const grey120 = '#DDE0E6';
const blue530 = '#2375E1';

export const TableBodyRow: any = styled.ul`
    position: relative;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    min-height: 68px;
    box-shadow: inset 0 -2px 0 0 ${grey120};
    padding: 0 20px;
    box-sizing: border-box;

    &:hover {
        /* box-shadow: 0 2px 12px rgba(45, 47, 51, 0.25); */
    }

    &.active-true {
        &::after {
            content: '';
            position: absolute;
            top: 16px;
            left: 0;
            width: 3px;
            height: 36px;
            background-color: ${blue530};
        }
    }
`;